<?php
declare(strict_types=1);

namespace N11t\Fake\AbstractCollection;

use N11t\AbstractCollection\AbstractCollection;

class FakeCollection extends AbstractCollection
{

    public function __construct(array $values = [])
    {
        $this->values = $values;
    }
}
