<?php
declare(strict_types=1);

namespace N11t\AbstractCollection;

/**
 * This abstract class can be used to create typed lists. Override the constructor and
 * set the values.
 *
 * Class AbstractCollection
 * @package N11t\AbstractCollection
 */
abstract class AbstractCollection implements \Countable, \IteratorAggregate
{

    /** @var array */
    protected $values = [];

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return \count($this->values);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->values);
    }

    /**
     * Return values as array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->values;
    }

    /**
     * Sort values by given compare function.
     *
     * See {@see \usort} for more information.
     *
     * @param callable $cmpFunction
     * @return bool
     */
    public function usort(callable $cmpFunction): bool
    {
        return usort($this->values, $cmpFunction);
    }

    /**
     * Filter values by given callback.
     *
     * See {@see \array_filter} for more information.
     *
     * @param callable $callback
     * @param int $flag [optional]
     * @return self
     */
    public function filter(callable $callback, int $flag = 0): self
    {
        $clone = clone $this;

        $clone->values = \array_filter($this->values, $callback, $flag);

        return $clone;
    }
}
