<?php
declare(strict_types=1);

namespace N11t\AbstractCollection;

use N11t\Fake\AbstractCollection\FakeCollection;
use PHPUnit\Framework\TestCase;

class AbstractCollectionTest extends TestCase
{

    public function testCanBeCounted()
    {
        // Arrange
        $collection = new FakeCollection([
            'asdf',
            'asdf',
            'asdf'
        ]);

        // Assert
        $actualCount = \count($collection);

        self::assertSame(3, $actualCount);
    }

    public function testCanBeIteratedInForeach()
    {
        // Arrange
        $collection = new FakeCollection([
            'asdf',
            'asdf',
            'asdf'
        ]);

        // Act
        $count = 0;
        foreach ($collection as $string) {
            self::assertSame('asdf', $string);
            $count++;
        }

        // Assert
        self::assertSame(3, $count);
    }

    public function testCanBeConvertedToArray()
    {
        // Arrange
        $array = [
            'asdf1',
            'asdf2',
            'asdf3'
        ];
        $collection = new FakeCollection($array);

        // Assert
        self::assertEquals($array, $collection->toArray());
    }

    public function testCanBeSortedWithCompareFunction()
    {
        // Arrange
        $array = [
            'asdf3',
            'asdf1',
            'asdf2'
        ];
        $collection = new FakeCollection($array);

        // Act
        $collection->usort(function (string $a, string $b) {
            return $a <=> $b;
        });

        // Assert
        self::assertEquals([
            'asdf1',
            'asdf2',
            'asdf3'
        ], $collection->toArray());
    }

    public function testCanBeFiltered()
    {
        // Arrange
        $array = [
            1 => 'asdf',
            2 => 'fdsa',
            3 => '1234',
            4 => '4321',
            5 => 'qwert',
            6 => 'trewq',
            7 => '1094',
            8 => '67890'
        ];
        $collection = new FakeCollection($array);

        // Act
        $newCollection = $collection->filter(function (string $value) {
            return \preg_match('/^\d{4}$/', $value) === 1;
        });

        // Assert
        self::assertEquals([
            3 => '1234',
            4 => '4321',
            7 => '1094'
        ], $newCollection->toArray());
    }

    public function testCanBeFilteredWithUseKeyFlag()
    {
        // Arrange
        $array = [
            1 => 'asdf',
            2 => 'fdsa',
            3 => '1234',
            4 => '4321',
            5 => 'qwert',
            6 => 'trewq',
            7 => '1094',
            8 => '67890'
        ];
        $collection = new FakeCollection($array);

        // Act
        $newCollection = $collection->filter(function (string $value) {
            return $value > 4;
        }, \ARRAY_FILTER_USE_KEY);

        // Assert
        self::assertEquals([
            5 => 'qwert',
            6 => 'trewq',
            7 => '1094',
            8 => '67890'
        ], $newCollection->toArray());
    }

    public function testCanBeFilteredWithUseBothFlag()
    {
        // Arrange
        $array = [
            1 => 'asdf',
            2 => 'fdsa',
            3 => '1234',
            4 => '4321',
            5 => 'qwert',
            6 => 'trewq',
            7 => '1094',
            8 => '67890'
        ];
        $collection = new FakeCollection($array);

        // Act
        $newCollection = $collection->filter(function (string $value, int $key) {
            return $key % 2 === 0 || $value === 'qwert';
        }, \ARRAY_FILTER_USE_BOTH);

        // Assert
        self::assertEquals([
            2 => 'fdsa',
            4 => '4321',
            5 => 'qwert',
            6 => 'trewq',
            8 => '67890'
        ], $newCollection->toArray());
    }
}
